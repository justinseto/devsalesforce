<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>DevOps_test_email</fullName>
        <description>DevOps test email</description>
        <protected>false</protected>
        <recipients>
            <recipient>jseto@deloitte.ca</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SalesNewCustomerEmail</template>
    </alerts>
    <alerts>
        <fullName>send_email_to_Justin</fullName>
        <description>send email to Justin</description>
        <protected>false</protected>
        <recipients>
            <recipient>jseto@deloitte.ca</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SalesNewCustomerEmail</template>
    </alerts>
    <rules>
        <fullName>Create Opportunity</fullName>
        <actions>
            <name>DevOps_test_email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>test</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
